Performance Notes
=================

Docker `run` command runs a clean and up-to-date container, but it takes unexpected amount of time
to run as it checks for an image update and creates a new container each time.

Thus, we moved to using `start` command, i.e. we prepare (using `run` command) all our containers
on the initialization step of our Testing System and `start` prepared containers in the read-only
mode (more details in Security notes section).

Even though `start`ing prepared containers is faster than `run`ning them, it still takes
`0.6s` per run (60 seconds to run a zero-time solution on 100 tests)! So we have decided to run
solutions in a batch mode, i.e. start a container to run a solution against all tests, which will
take only 0.6 extra seconds per solution.
