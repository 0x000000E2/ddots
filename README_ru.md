Testing System for Dots
=======================

Обзор
-----

DDots - это тестирующая система для Dots, написаная на Python, sh и C и интенсивно использующая
все возможности Docker контейнеров и Cgroup подсистемы ядра, запуская компиляторы и тестируя
пользовательские решения в отдельных Docker контейнерах. Такой подход делает DDots максимально
гибкой и модульной системой.

Один DDots обрабатывает одно решение в единицу времени и предзагружает два решения в очередь для
уменьшения задержек, связанных с сетью. Таким образом, для нагрузки нескольких ядер рекомендуется
запускать N контейнеров DDots (это делается автоматически, Makefile автоматически определяет
количество доступных ядер и использует все возможности).

Больше информации доступно в английской версии README и в Wiki страницах.


Установка
---------

Доступны инструкции для следующих вариантов развёртывания DDOTS:

* Boot2Docker - идеальный вариант для развёртывания в виртуальных машинах (этот вариант уже
  проверен временем) ([инструкции](INSTALL_ON_BOOT2DOCKER_ru.md))
* Ubuntu - отличный вариант для установки "на железо" (известные проблемы были решены с
  использованием Linux Kernel 4.0+ с поддержкой AUFS, так что Ubuntu 16.04+ должен работать без
  проблем) ([инструкции](INSTALL_ON_UBUNTU_ru.md))



Обновление DDOTS
----------------

ПРИМЕЧАНИЕ: Скрипт автозапуска в boot2docker делает автоматическое обновление на старте, так что
можно просто перегрузить систему.

В общем же случае, когда DDots уже установлен, его обновление производится в одну команду:

```bash
$ make update
```

Перегрузите DDOTS после обновления.


Usage
-----

ВНИМАНИЕ: boot2docker требует активации окружения (подробности выше)!

### DDots

Запуск:

```bash
$ make start
```

Note: You can use `DDOTS_CPU_COUNT` env variable to control the number of ddots
instances. By default, it will create an instance for each core.

Логи:

```bash
$ make logs
```

Остановить:

```bash
$ make stop
```

Перезапуск:

```bash
$ make restart
```

### Checkers

Компиляция checker'a (`check.cpp` в `check`):

```bash
$ docker run -it --rm --volume `pwd`:/mnt --workdir /mnt frolvlad/alpine-gxx sh -c 'c++ --static -s -O3 -I. check.cpp -o check ; chown 1000:1000 check'
```

Массовая перекомпиляция checker'ов в KHCUP соревнованиях (`0[0-9][0-9]/*cmp.cpp` в `check`):

```bash
$ cd problems
$ find . \
    -name '0[0-9][0-9]' \
    -type d \
    -print \
    -exec \
        docker run \
            -it \
            --rm \
            --volume "$(pwd)/{}:/mnt" \
            --workdir /mnt \
            frolvlad/alpine-gcc \
                sh -c '\
                    c++ --static -s -O3 -I. *cmp.cpp -o check ; \
                    chown 1000:1000 check ; \
                ' \
        \;
```

### Ручное тестирование

ВНИМАНИЕ: boot2docker требует активации окружения (подробности выше)!

Тестировать одно конкретное решение:

```bash
$ #export PROGRAMMING_LANGUAGE=(gcc|gcc-c11|gpp|gpp-cpp11|fpc|fpc-delphi|openjdk7|oraclejdk7|mono|go|python2|python3|...)
$ export PROGRAMMING_LANGUAGE=gpp

$ #export PROBLEM_ROOT=<path to the tests root>
$ export PROBLEM_ROOT=/tmp/tests

$ #export SOLUTION=<path to the solution source>
$ export SOLUTION=/tmp/qq.cpp

$ #export TESTING_MODE=(full|first_fail|one)
$ export TESTING_MODE=full

$ make test_solution
```

ИЛИ можно записать это всё в одну строку:

```bash
$ PROGRAMMING_LANGUAGE=gpp PROBLEM_ROOT=/tmp/tests SOLUTION=/tmp/qq.cpp TESTING_MODE=full make test_solution
```


Файлы и директории
------------------

* `/tmp/ddots/problems_db` - эта директория существует внутри каждого
контейнера ddots-testing-system-*, тесты загружаются автоматически по мере
необходимости и очищаются при перезапуске контейнера;
* `./testing_system/local_config.py` - файл настроек
* `./examples/problems_db/` - здесь есть хороший пример типичной задачи DOTS
