Установка самого DDOTS
======================

ВАЖНО: Пожалуйста, подготовьте все зависимости как это описано в 
[README](README_ru.md) (и соответствующих `INSTALL_ON_*` инструкциях) перед
продолжением.


Устанавливаем SSH ключи
-----------------------

```bash
$ bash -c 'set -x -e
mkdir -p ~/.ssh

cat > ~/.ssh/ddots_id_rsa << 'EOF'
-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEAvLCylieY4Svd3ekWWO4lcgUEjtj5Cl4Byiz43AHLz9Fy+Ikv
L6ix+H5WWi7wc8i0A816Mwa8ZH9CDwAVoo09SrBniWm9OVbycSfv+ipDTNukgLBt
DTmnwUDtqB4ybcAiKfenVct5k5arHe3VXYgWCnvnta48/to8uZVMO85N6JPdPkNz
/dFA4XTbG9SkCvoCmSDMZTCCHwMCBXAuwPM5BT8e7rcpYm615N1M4QonglT/dLU4
9oFOa6ko2yMAfQttSActFeVuIhmE5a3JXAiYh05DQ2d/5Vji3f4wUdOF0K/dtRV8
uH3fiDfPzHrvmLp1/kiA8FkBq8USaEO2FslbgQIDAQABAoIBAQClUT62dezo43EY
ut4cflIKasEKkqjnX2O8CdjjkSNLaduMS00vOWLsL2xiyRtcpiyp5D47NJychC6R
S+aoq4xWwuWDrEPgWABECdSkXqDmVs3y/iAdZTVICSXvlL1kEOEd2eAfOA3FtE0q
BLVapB+DgfmJ4SWJuBVaF6daAq+Zo2jti57tb0YEVAB7RAwvo6mcVauglkXf9OFG
gVkLscTpKtzJWIEG0RLBruxYWwClukrk/t40B9mVM+8AHg8m9/Mykrm7JYLPIKUt
CdFIRWZVnkv6ZqgQmDgwJI//Yi+zk/qLGldcep2lXGAm+/Gsv+uHvqNu69C7jNsL
ka4SMjotAoGBAO33ixcQME5EMgrBvxArJypZpXcypj1r9ySPuf9CqfNDQeWukRaB
Y00J4NualnmZfhu+xcCYrdY9Lt7xj+ZCrbg7HUYYXYFZ66x0vwdn9/60FZxdDFCe
7tLZx+SS/agIVCrOpsoBEYsomcXbNEACL3O4WPOCWbRydj3gzAsEanrfAoGBAMr9
NOvTbzPlkpS4B82vHHh0IWI3B7xysIzpYiwiymeLUhKbtMzzhRwGIcpztCF4K4oV
5ne5ldQefeh322Ukrf6M2XC52Jit5PvrIUeHPWa0USKXko1Y6mZwAVi9+V+C5+PS
h8eAq0KWvdfZow/nyWmhbUxEUYs52EOMGLfYhVWfAoGBAMpohCZZ3ryzxNzhb2KR
Uf7zwYphyoT0EINIvSwhx+ziiGqU0VQTOiONA3XHR7ammF2BJzNqqnJQGE4Jqhxx
GtJZdczpbJ9ZnNxTcSkbhhVfmAIoIxFwHjO6igwh+x+x9eVzYOrxfbqgDiv9B7Rq
J3KGmyKE/Ug/E9QOIYA9gVSBAoGBAJKgjNW8iRtHvbZHqPtefoZT/LlhXuk1P2rK
Ouv+xY4+CS7/4n4ZehFgLJB9O6ThlTHN2AiruhrbW7/MtGI/OKAjRf4zgM3o7vs6
QXPVsPi7T9BZBLetfsQ4LADaWYQwnyjmLsZhn1QeZ0vAHhWjLM5smPL6dpzHT/ya
+k/yy32pAoGAQR33odObml2EYdXIVxJgpOhxdjXrCJNFSs8Vag3aCNA2V0UoJWyl
Dh5tW+R8DexLmlLJrTQ34tYzl/nQELTTJIalkohXzQKP2qdzMrqcSXFp3tdQfNhP
nvTVRGLLCV5Zii1PIXvMTo9LZLqwzP+sgy+QZpj7IrM5EcOR9uBlnnM=
-----END RSA PRIVATE KEY-----
EOF

chmod 600 ~/.ssh/ddots_id_rsa

cat >> ~/.ssh/config << 'EOF'
Host gitlab.prostoksi.com
IdentityFile "~/.ssh/ddots_id_rsa"
EOF
'
```


Скачиваем DDOTS
---------------

```bash
$ git clone --recurse-submodules git@gitlab.prostoksi.com:ddots/ddots-testing-system.git
```


Инициализируем DDOTS
--------------------

На этом этапе скачивается порядка 2ГБ образов (компиляторы), которые распаковываются в 3.3ГБ):

```bash
$ cd ddots-testing-system
$ make install
```


Настраиваем DDOTS
-----------------

Копируем шаблон `testing_system/local_settings.py.template` в `testing_system/local_settings.py`
и редактируем его (ссылка на DOTS, логин/пароль DOTS, time factor и другое).
