# encoding: utf-8

import logging
import os
import tempfile
import threading
import time

import requests

from .extensions import dots_api, compilers_manager, runners_manager, solutions_queue_manager

from .exceptions import TestingSystemRestartSignal, TestingSystemError, CompilationError
from .utils import log_response


log = logging.getLogger(__name__)  # pylint: disable=invalid-name


class Config(object):
    # pylint: disable=invalid-name

    def __init__(self):
        self.DOTS_LANG_ID_TO_COMPILER = {}
        self.DOTS_LANG_ID_TO_RUNNER = {}

    def from_object(self, obj):
        for config_key in dir(obj):
            if config_key.isupper():
                setattr(self, config_key, getattr(obj, config_key))
        self.is_valid()

    def is_valid(self):
        if getattr(self, 'DOTS_USERNAME') is None:
            raise Exception("DOTS_USERNAME is a required setting.")
        if getattr(self, 'DOTS_PASSWORD') is None:
            raise Exception("DOTS_PASSWORD is a required setting.")
        assert \
            self.PREFETCH_SOLUTIONS_COUNT >= 2, \
            "Prefetching logic doesn't support prefetching less than 2 solutions"
        return True

    @property
    def AVAILABLE_COMPILERS(self):
        return set(self.DOTS_LANG_ID_TO_COMPILER.values())

    @property
    def AVAILABLE_RUNNERS(self):
        return set(self.DOTS_LANG_ID_TO_RUNNER.values())


class Worker(object):

    def __init__(self):
        self.config = Config()
        self._break_event = threading.Event()

    def _compile_solution(self, compiler_name, problem_path, solution_source_filename):
        log.info("Compiler '%s' is compiling the solution...", compiler_name)
        try:
            runnable_solution_filepath = compilers_manager.start(
                    compiler_name,
                    problem_path,
                    solution_source_filename
                )
        except TestingSystemError:
            log.exception("Testing System failed unexpectantly while compiling")
            raise
        log.info("The solution has been compiled")
        return runnable_solution_filepath

    def test_solution(self, programming_language_id, solution_source, problem_path, testing_mode):
        """
        Compile a solution if needed and run it against the problem tests.

        Args:
            programming_language_id (str): a unique programming language ID
            solution_source (str): a solution source text
            problem_path (str): a path to the problem files (Problem.xml,
                tests, etc)
            testing_mode (str): choice of ``full``, ``first_fail``, or ``one``
                (see also ``RunnersManager.TESTING_MODES``).

        Returns:
            report (str): a testing report in a custom space-separated format.
        """
        try:
            os.makedirs(self.config.SOLUTIONS_TEMP_ROOT)
        except FileExistsError:
            pass

        with tempfile.NamedTemporaryFile(
                    buffering=0,
                    dir=self.config.SOLUTIONS_TEMP_ROOT
                ) as tmp_solution_source_file:
            log.debug("The solution is being saved to disk")
            tmp_solution_source_file.write(solution_source)

            try:
                if programming_language_id in self.config.DOTS_LANG_ID_TO_COMPILER:
                    runnable_solution_filepath = self._compile_solution(
                            self.config.DOTS_LANG_ID_TO_COMPILER[programming_language_id],
                            problem_path,
                            tmp_solution_source_file.name
                        )
                else:
                    runnable_solution_filepath = tmp_solution_source_file.name

                log.info("The solution is being tested...")
                report = runners_manager.start(
                        self.config.DOTS_LANG_ID_TO_RUNNER[programming_language_id],
                        problem_path,
                        testing_mode,
                        runnable_solution_filepath,
                    )
            except CompilationError as exc:
                report = runners_manager.truncate_report(
                        '1 CE 0 0 0\n\n%s' % exc
                    )
            except Exception:
                log.exception("Testing System failed unexpectantly")
                raise TestingSystemRestartSignal()

        report += '\n--\nDdotS version: 1.0.0'
        log.debug("Testing report is:\n    %s", '\n    '.join(report.split('\n')))
        log.info("The solution has been tested")
        return report

    def run_worker_loop(self):
        """
        The entry-point and the main process of DDOTS.
        """
        log.info("Processor loop is starting...")

        log.debug("Getting a session ID...")
        try:
            dots_api.authenticate(
                    username=self.config.DOTS_USERNAME,
                    password=self.config.DOTS_PASSWORD,
                )
        except requests.ConnectionError:
            raise TestingSystemRestartSignal()

        solutions_queue_manager.start()
        log.info("Processor loop is waiting for new solutions at '%s'...", dots_api.base_url)

        while 1:
            try:
                solution_info = solutions_queue_manager.get_solution()
            except StopIteration:
                raise TestingSystemRestartSignal()

            # Confirm (lock/checkout) the received solution
            solution_lock_response = dots_api.get('/lock/%s' % solution_info['solution_name'])
            log_response(log.debug, "SOLUTION LOCK RESPONSE", solution_lock_response)
            if solution_lock_response.status_code != 200:
                log_response(
                        log.error,
                        "Something went wrong while locking the solution '%s'.",
                        solution_lock_response,
                        solution_info['solution_name']
                    )
                continue
            log.info("Solution '%s' has been locked", solution_info['solution_name'])

            try:
                report = self.test_solution(
                        solution_info['programming_language_id'],
                        solution_info['solution_source'],
                        solution_info['problem_path'],
                        solution_info['testing_mode']
                    )
            except TestingSystemError:
                # Rollback (unlock) the solution
                solution_unlock_response = dots_api.get(
                        '/unlock/%s' % solution_info['solution_name']
                    )
                if solution_unlock_response.status_code != 200:
                    log_response(
                            log.warning,
                            "Unlocking the solution '%s' has failed.",
                            solution_unlock_response,
                            solution_info['solution_name']
                        )
                else:
                    log.info(
                            "Solution '%s' has been successfully unlocked.",
                            solution_info['solution_name']
                        )
                continue

            log.debug(
                    "Sending report to '/result/%s': %r",
                    solution_info['solution_name'],
                    report.encode('utf-8')
                )
            post_result_response = dots_api.get(
                    '/result/%s' % solution_info['solution_name'],
                    data=report.encode('utf-8')
                )
            if post_result_response.status_code != 200:
                log_response(
                        log.error,
                        "Posting the result testing report has failed.",
                        post_result_response
                    )
                continue
            if post_result_response.text != '1 OK':
                log_response(
                        log.error,
                        "The testing report has been declined.",
                        post_result_response
                    )
                continue
            log.info(
                    "The testing report for solution '%s' has been sent",
                    solution_info['solution_name']
                )

            log.info("Solution '%s' has been tested successfully", solution_info['solution_name'])

    def run(self):
        """
        This is the worker supervisor.
        """
        while not self._break_event.is_set():
            try:
                self.run_worker_loop()
            except TestingSystemRestartSignal:
                if self._break_event.is_set():
                    log.info("Soft shutdown")
                    break
                log.exception("Soft restart")
            except requests.exceptions.ConnectionError as exception:
                log.warning("Connection Error: %s", exception)
            except Exception:  # pylint: disable=broad-except
                log.exception("Something went wrong. Restarting in 10 seconds...")
                time.sleep(9)
            time.sleep(1)

    def stop(self):
        log.warning("Stopping the worker...")
        self._break_event.set()
        solutions_queue_manager.stop()
