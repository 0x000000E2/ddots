# encoding: utf-8
# pylint: disable=invalid-name,wrong-import-position

from .api import DOTSApi
dots_api = DOTSApi()

from .problems import ProblemsManager
problems_manager = ProblemsManager()

from .compilers import CompilersManager
compilers_manager = CompilersManager()

from .runners import RunnersManager
runners_manager = RunnersManager()

from .solutions import SolutionsQueueManager
solutions_queue_manager = SolutionsQueueManager()


def init_app(app):
    for extension in (
            dots_api,
            problems_manager,
            compilers_manager,
            runners_manager,
            solutions_queue_manager,
        ):
        extension.init_app(app)
