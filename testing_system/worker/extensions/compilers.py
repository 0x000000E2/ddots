# encoding: utf-8
"""
Solution Compilators module
===========================
"""
import logging
import os
import shutil

from ..exceptions import TestingSystemError, DockerError, CompilationError, ContainerTimeout
from .docker import DockerManager
from .problems import ProblemsDockerManagerMixin


log = logging.getLogger(__name__)  # pylint: disable=invalid-name


class CompilersManager(ProblemsDockerManagerMixin, DockerManager):
    """
    Docker-based Compilers Manager
    """

    DOCKER_IMAGE_PREFIX = 'ddots-compiler-'

    DEFAULT_CONTAINER_TIMEOUT = 20

    COMPILATION_ROOT_CHMOD = 0o777

    SOLUTION_SOURCE_FILENAME = 'solution.source'
    SOLUTION_EXECUTABLE_FILENAME = 'solution.bin'

    def init_app(self, app):
        super(CompilersManager, self).init_app(app)
        self._base_container_name = app.config.BASE_CONTAINER_NAME
        self._solution_source_filepath = os.path.join(
                self._shared_volumes['data']['mount_point'],
                self.SOLUTION_SOURCE_FILENAME
            )
        self._solution_executable_filepath = os.path.join(
                self._shared_volumes['sandbox']['mount_point'],
                self.SOLUTION_EXECUTABLE_FILENAME
            )
        self.init_containers_list(set(app.config.DOTS_LANG_ID_TO_COMPILER.values()))

    def _get_container_name(self, container_suffix):
        return '%s%s-%s' % (self.DOCKER_IMAGE_PREFIX, self._base_container_name, container_suffix)

    def _get_container_init_options(self, container_suffix, *args, **kwargs):
        container_init_options = super(CompilersManager, self)._get_container_init_options(
                container_suffix,
                *args,
                **kwargs
            )
        container_init_options.update({
                'user': 'operator',
                'network_disabled': True,
                'entrypoint': '/bin/sh',
                'command': (
                        '/usr/local/bin/compile.sh',
                        self._solution_source_filepath,
                        self._solution_executable_filepath,
                        os.path.join(
                                self._shared_volumes['data']['mount_point'],
                                self.CURRENT_PROBLEM_DIRNAME
                            ),
                    ),
            })
        container_init_options['environment']['PROGRAMMING_LANGUAGE_CODE'] = container_suffix
        return container_init_options

    def init_containers_list(self, containers_suffix_list):
        log.info("Initializing compiler containers...")
        super(CompilersManager, self).init_containers_list(containers_suffix_list)

    def _prepare_environment(self, problem_path, problem_info, solution_source_path):
        # pylint: disable=arguments-differ
        # Copy a solution to a predefined path
        try:
            shutil.copyfile(solution_source_path, self._solution_source_filepath)
        except FileNotFoundError:
            raise TestingSystemError("solution not found")

    def start(self, container_suffix, problem_path, solution_source_path, **kwargs):
        """
        This function prepares source file and runs a compiler against it.

        In ddots we have separate Docker containers for each compiler.

        The workflow of this function:
            1. Copy a solution file to a predefined directory with a predefined
            filename ('solution.source').
            2. Start a precreated compiler container, which runs a compiler itself
            and stores a compiled file in a predefined directory with a predefined
            filename ('solution.bin')
            3. Return a path to the executable
        """
        self.prepare_environment(
                problem_path,
                solution_source_path=solution_source_path,
            )
        # Compile
        try:
            compiler_exit_code, compiler_output = super(CompilersManager, self).start(
                    container_suffix,
                    **kwargs
                )
        except ContainerTimeout:
            log.error(
                    "Compilation process has hung and we return CE to the user "
                    "because it is the best what we can report in such a case."
                )
            raise CompilationError(
                    "%s: Compiler has probably hang. Please, try once again if "
                    "you think your code is fine." % self.SOLUTION_SOURCE_FILENAME
                )
        except DockerError:
            log.exception("Compilation process has crashed with the following error:")
            raise

        if compiler_exit_code != 0:
            if any(compiler_output):
                compilation_error_message = '\n'.join(compiler_output)
            else:
                compilation_error_message = (
                    "%s: Compiler has probably crashed." % self.SOLUTION_SOURCE_FILENAME
                )
            raise CompilationError(compilation_error_message)

        # compiler must always store the compilation result in
        # `executable_filepath` path.
        if not os.path.exists(self._solution_executable_filepath):
            raise TestingSystemError(
                "solution is reported as compiled, but there is no executable"
            )
        return self._solution_executable_filepath
