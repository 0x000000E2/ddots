# encoding: utf-8
"""
DOTS Problems module
====================
"""
import logging
import os
import shutil
import tarfile

import lockfile
import requests
import xmltodict

from ..exceptions import TestingSystemError
from ..utils import download_file
from . import dots_api


log = logging.getLogger(__name__)  # pylint: disable=invalid-name


def download_problem_targz_file(url, local_path, lock_timeout=10, http_timeout=None, **kwargs):
    """
    Download and extract problem files (Problem.xml, tests, and a checker).

    Args:
        url (str): URL to the remote problem .tar.gz archive.
        local_path (str): the destination path.
        lock_timeout (float): the problem files can be requested concurrently
            and we might just need to wait until the other process finishes the
            download.
        http_timeout (float): timeout for HTTP requests.

    Returns:
        local_path (str): the folder to which the problem files were extracted.
    """
    log.debug("Checking existance of a problem in '%s'...", local_path)
    lock = lockfile.LockFile(local_path)
    try:
        lock.acquire(timeout=lock_timeout)
    except lockfile.LockTimeout:
        log.info(
            "Folder '%s' is locked. Probably another instance is still downloading it.",
            local_path
        )
        raise
    try:
        if not os.path.exists(local_path):
            log.info("Getting a missing problem '%s'...", local_path)
            problem_targz_file = download_file(
                url,
                '%s.tar.gz' % local_path,
                lock_timeout=lock_timeout,
                http_timeout=http_timeout,
                **kwargs
            )
            local_root, local_name = os.path.split(local_path)
            local_name_prefix = '%s/' % local_name
            log.info("Unpacking a problem to '%s'...", local_path)
            # Problem archives should always contain only one folder with everything else inside it,
            # but just to make sure that wrong archive won't break our problems_db we make this
            # 'complex' extraction.
            with tarfile.open(problem_targz_file) as problem_archive:
                problem_archive.extractall(
                    path=local_root,
                    members=(
                        tarinfo for tarinfo in problem_archive \
                            if tarinfo.name.startswith(local_name_prefix)
                    )
                )
        log.debug("The problem in '%s' is ready to use", local_path)
        return local_path
    finally:
        lock.release()


class ProblemsManager(object):
    """
    Local problems DB manager.
    """
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self._problems_db_root = app.config.PROBLEMS_DB_ROOT
        self._problem_download_timeout = app.config.DOTS_PROBLEM_DOWNLOAD_TIMEOUT
        try:
            os.makedirs(self._problems_db_root)
        except FileExistsError:
            pass

    def get_problem_path(self, problem_id):
        """
        Args:
            problem_id (str): a unique ID of a problem.

        Returns:
            problem_path (str): a problem path in the problems DB.
        """
        return os.path.join(self._problems_db_root, problem_id)

    def prepare_problem(self, problem_id):
        """
        Ensure that the problem files exist and ready to be used.

        Args:
            problem_id (str): a unique ID of a problem.

        Returns:
            problem_path (str): a problem path in the problems DB.
        """
        problem_path = self.get_problem_path(problem_id)
        if os.path.exists(problem_path):
            return problem_path

        try:
            download_problem_targz_file(
                '/tests/%s' % problem_id,
                problem_path,
                lock_timeout=self._problem_download_timeout,
                http_timeout=self._problem_download_timeout,
                session=dots_api
            )
        except lockfile.LockTimeout:
            raise TestingSystemError(
                    "problem #%s folder has been locked for too long" % problem_id
                )
        except requests.HTTPError:
            raise TestingSystemError("problem #%s failed to download" % problem_id)
        return problem_path


class ProblemsDockerManagerMixin(object):
    """
    Helper DockerManager Mixin helping to prepare current problem files
    available to the Docker containers.
    """

    PROBLEM_XML_NAME = 'Problem.xml'

    CURRENT_PROBLEM_DIRNAME = 'problem'
    CURRENT_PROBLEM_ROOT_CHMOD = 0o770

    def get_problem_info(self, problem_path):
        """
        Args:
            problem_path (str): a path to a problem folder which contains
                ``Problem.xml``.

        Returns:
            problem_info (dict): information extracted from the
            ``Problem.xml``.
        """
        # Parse Problem.xml to get information about tests
        with open(os.path.join(problem_path, self.PROBLEM_XML_NAME)) as problem_xml_file:
            return xmltodict.parse(problem_xml_file.read())['Problem']

    def _prepare_environment(self, problem_path, problem_info, **kwargs):
        pass

    def prepare_environment(self, problem_path, **kwargs):
        """
        Prepare an environment (folders, files, permissions, etc) before a
        container execution.
        """
        problem_info = self.get_problem_info(problem_path)

        # Prepare problem root
        current_problem_root = os.path.join(
                self._shared_volumes['data']['mount_point'],
                self.CURRENT_PROBLEM_DIRNAME
            )
        try:
            shutil.rmtree(current_problem_root)
        except FileNotFoundError:
            pass
        shutil.copytree(problem_path, current_problem_root)

        # chmod files in `current_problem_root` recursively
        os.chmod(current_problem_root, self.CURRENT_PROBLEM_ROOT_CHMOD)
        executables = (problem_info['@CheckerExe'], problem_info.get('@PatcherExe'))
        for root, dirs, files in os.walk(current_problem_root):
            for _dir in dirs:
                os.chmod(os.path.join(root, _dir), self.CURRENT_PROBLEM_ROOT_CHMOD)
            for _file in files:
                if _file in executables:
                    os.chmod(os.path.join(root, _file), self.CURRENT_PROBLEM_ROOT_CHMOD)
                else:
                    os.chmod(os.path.join(root, _file), self.CURRENT_PROBLEM_ROOT_CHMOD & 0o666)

        self._prepare_environment(problem_path, problem_info, **kwargs)
