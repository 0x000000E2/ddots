# encoding: utf-8

import logging
import time

import requests

from ..utils import log_response


log = logging.getLogger(__name__)  # pylint: disable=invalid-name


class DOTSApi(object):

    MAX_RETRIES = 10
    RETRIES_DELAY = 2.0
    DEFAULT_HTTP_TIMEOUT = 30.0

    def __init__(self, app=None):
        self.session = requests.Session()
        self.session.mount('', requests.adapters.HTTPAdapter(max_retries=self.MAX_RETRIES))
        self.access_token = None
        self.base_session_url = None

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.base_url = app.config.DOTS_API_URL
        if not app.config.DOTS_API_USE_KEEP_ALIVE_CONNECTIONS:
            # KHCup DOTS setup has issues with keep-alive connections, so as a
            # workaround we ask server to close connections instead of keeping
            # them alive.
            self.session.headers['Connection'] = 'close'

    def authenticate(self, username, password):
        """
        Get DOTS Session ID
        """
        self.session.auth = (username, password)
        auth_response = self.session.get(
            '%s/bot/' % self.base_url,
            timeout=self.DEFAULT_HTTP_TIMEOUT
        )

        if auth_response.status_code != 200:
            log_response(log.error, "Cannot get a session ID.", auth_response)
            # Raise proper ConnectionError or HTTPError
            auth_response.raise_for_status()

        if 'DSID' not in auth_response.cookies:
            log.error("DSID is not set in the session response")
            raise requests.ConnectionError("DSID is not set")

        self.access_token = auth_response.cookies['DSID']
        log.debug("Session ID is %s", self.access_token)

        self.base_session_url = '%s/sess/%s/bot' % (self.base_url, self.access_token)

    def get(self, url_suffix, timeout=DEFAULT_HTTP_TIMEOUT, **kwargs):
        for retries_left in range(self.MAX_RETRIES, 0, -1):
            try:
                return self.session.get(
                    self.base_session_url + url_suffix,
                    timeout=timeout,
                    **kwargs
                )
            except requests.ConnectionError as exc:
                if retries_left == 1:
                    raise exc
            time.sleep(self.RETRIES_DELAY)
