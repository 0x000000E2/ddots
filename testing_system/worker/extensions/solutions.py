# encoding: utf-8
"""
DOTS solutions handlers module
==============================
"""

import logging
import queue
import re
import time
import threading

import requests

from ..exceptions import TestingSystemError
from ..utils import log_response
from . import dots_api, problems_manager, runners_manager


log = logging.getLogger(__name__) # pylint: disable=invalid-name


TESTING_MODE_CODE_TO_NAME_MAPPING = {
    'F': runners_manager.TM_FULL,
    'A': runners_manager.TM_FIRST_FAIL,
    'S': runners_manager.TM_ONE,
    'T': runners_manager.TM_ONE,
}

# Solution name example: 172484_5046.1001.03F
SOLUTION_NAME_REGEX = re.compile(
    r'^(?P<solution_id>\d+)_' \
        r'(?P<problem_id>\d+)\.'
        r'(?P<user_id>\d+)\.'
        r'(?P<programming_language_id>\d+)(?P<testing_mode_code>[%s])$' % (
            ''.join(TESTING_MODE_CODE_TO_NAME_MAPPING.keys())
        )
)

class SolutionsQueuePrefetchThread(threading.Thread):
    """
    New Solutions Queue manager.

    This manager gets new solutions from DOTS and provide an in-memory queue of those it has
    prefetched while system is processing previously downloaded ones.
    """

    ERROR_RETRIES_COUNT = 3

    def __init__(
            self,
            new_solutions_queue,
            break_event,
            supported_programming_language_ids,
            delay_between_checks
        ):
        self.__new_solutions_queue = new_solutions_queue
        self.__break_event = break_event
        self.__supported_programming_language_ids = supported_programming_language_ids
        self.__delay_between_checks = delay_between_checks
        super(SolutionsQueuePrefetchThread, self).__init__()

    def _check_new_solution(self):
        # pylint: disable=too-many-return-statements
        new_solution_response = dots_api.get('/solution/')
        log_response(log.debug, "SOLUTION RESPONSE", new_solution_response)
        if new_solution_response.status_code != 200:
            log_response(
                    log.warning,
                    "Something went wrong while checking for new solutions.",
                    new_solution_response
                )
            return

        solution_name = new_solution_response.text
        if solution_name == '0 no solutions':
            log.debug("No solutions")
            return
        log.info("New solution '%s' has arrived", solution_name)

        try:
            solution_info = SOLUTION_NAME_REGEX.search(solution_name).groupdict()
        except AttributeError:
            log.warning("solution_name '%s' has wrong format", solution_name)
            return

        programming_language_id = solution_info['programming_language_id']
        if programming_language_id not in self.__supported_programming_language_ids:
            log.info(
                    "Programming language id '%s' is not supported",
                    programming_language_id
                )
            return

        # Download and unpack tests for the problem if necessary
        try:
            problem_path = problems_manager.prepare_problem(solution_info['problem_id'])
        except TestingSystemError:
            # Rollback a solution
            solution_response = dots_api.get('/unlock/%s' % solution_name)
            return

        # Receive a solution
        solution_response = dots_api.get('/solution/%s' % solution_name)
        log_response(log.debug, "SOLUTION RESPONSE", solution_response)
        if solution_response.status_code != 200:
            log_response(
                    log.error,
                    "Something went wrong while receiving the solution '%s'.",
                    solution_response,
                    solution_name
                )
            return
        log.info("Solution '%s' has been received", solution_name)

        solution_info.update({
                'solution_name': solution_name,
                'solution_source': solution_response.content,
                'problem_path': problem_path,
                'testing_mode': TESTING_MODE_CODE_TO_NAME_MAPPING[solution_info['testing_mode_code']],
            })
        return solution_info

    def run(self):
        """
        Check for new solutions to test and return a solution name once we get one.
        """
        try:
            error_retries_left = self.ERROR_RETRIES_COUNT
            while not self.__break_event.is_set():
                try:
                    time.sleep(self.__delay_between_checks)

                    new_solution = self._check_new_solution()
                    if new_solution:
                        self.__new_solutions_queue.put(new_solution)
                except requests.ConnectionError as exception:
                    log.warning("Connection Error: %s", exception)
                except Exception: # pylint: disable=broad-except
                    log.exception("Something went wrong. Retrying in 10 seconds...")
                    error_retries_left -= 1
                    if error_retries_left < 0:
                        raise
                    time.sleep(10.0)
                else:
                    error_retries_left = self.ERROR_RETRIES_COUNT
        finally:
            log.info("Exiting from a new solution checker...")
            self.__new_solutions_queue.put(StopIteration())
            # Avoid a refcycle if the thread is running a function with
            # an argument that has a member that points to the thread.
            del self.__new_solutions_queue
            del self.__break_event
            del self.__supported_programming_language_ids


class SolutionsQueueManager(object):
    # pylint: disable=too-few-public-methods
    """
    Helper Solutions Queue Manager.
    """

    def __init__(self, app=None):
        self._new_solutions_queue = None
        self._new_solutions_prefetch_thread = None
        self._break_event = threading.Event()
        self._started = False
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self._new_solutions_queue = queue.Queue(
                maxsize=min(app.config.PREFETCH_SOLUTIONS_COUNT - 1, 1)
            )
        self._supported_programming_language_ids = set(app.config.DOTS_LANG_ID_TO_RUNNER.keys())
        self._delay_between_checks = app.config.PREFETCH_DELAY_BETWEEN_CHECKS

    def __del__(self):
        self.stop()
        if self._new_solutions_queue is not None:
            if queue is None or queue.Empty is None:
                return
            try:
                while 1:
                    self._new_solutions_queue.get_nowait()
            except queue.Empty:
                pass

    def start(self):
        if not self._started:
            self._break_event.clear()
            if self._new_solutions_prefetch_thread is not None:
                self._new_solutions_prefetch_thread.join(timeout=30)
            self._new_solutions_prefetch_thread = SolutionsQueuePrefetchThread(
                    self._new_solutions_queue,
                    self._break_event,
                    supported_programming_language_ids=self._supported_programming_language_ids,
                    delay_between_checks=self._delay_between_checks
                )
            self._new_solutions_prefetch_thread.start()
            self._started = True

    def stop(self):
        if self._started:
            # TODO: Unlock the already fetched solutions
            self._break_event.set()
            self._started = False
            self._new_solutions_prefetch_thread.join(timeout=30)
            self._new_solutions_prefetch_thread = None

    def get_solution(self):
        log.debug("Locking until new solution will be received...")
        # This is a workaround needed to allow system signal handling.
        # http://stackoverflow.com/questions/39147669/handling-a-sigterm-when-blocked-in-queue-queue-get/
        while 1:
            try:
                new_solution = self._new_solutions_queue.get(timeout=1)
            except queue.Empty:
                pass
            else:
                break
        if isinstance(new_solution, dict):
            return new_solution
        if isinstance(new_solution, StopIteration):
            self._started = False
            raise new_solution
        raise TestingSystemError(
                "Received an unexpected type solution ('%s') from new_solution_queue" % new_solution
            )
