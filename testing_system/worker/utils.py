# encoding: utf-8

import logging
import os

import lockfile
import requests


log = logging.getLogger(__name__)  # pylint: disable=invalid-name


def log_response(logger, msg, response, *args, **kwargs):
    logger(msg + " Response details:\n"
            "    STATUS_CODE=%d\n"
            "    HEADERS=%s\n"
            "    CONTENT=%s",
        *(args + (
            response.status_code,
            response.headers,
            response.content
        )),
        **kwargs
    )


def download_file(
        url, local_filepath,
        chunk_size=1024*512, lock_timeout=10, http_timeout=None, session=None
    ):
    log.debug("Checking file existance in '%s'", local_filepath)
    lock = lockfile.LockFile(local_filepath)
    try:
        lock.acquire(timeout=lock_timeout)
    except lockfile.LockTimeout:
        log.info(
                "File '%s' is locked. Probably, another instance is still downloading it.",
                local_filepath
            )
        raise
    try:
        if not os.path.exists(local_filepath):
            log.info("Downloading a file from '%s' to '%s'", url, local_filepath)
            if session is None:
                session = requests
            response = session.get(url, stream=True, timeout=http_timeout)
            if response.status_code != 200:
                log_response(log.error, "Download '%s' is failed" % url, response)
                response.raise_for_status()
            with open(local_filepath, 'wb') as save_as_file:
                for chunk in response.iter_content(chunk_size=chunk_size):
                    # filter out keep-alive new chunks
                    if chunk:
                        save_as_file.write(chunk)
        log.debug("File '%s' has been downloaded", local_filepath)
        return local_filepath
    finally:
        lock.release()
