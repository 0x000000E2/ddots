# encoding: utf-8
"""
DDOTS worker module
===================
"""
import itertools
import logging

from .worker import Worker


log = logging.getLogger(__name__)  # pylint: disable=invalid-name
current_app = None


def test_solution(programming_language, solution_source_filepath, problems_root, testing_mode):
    """
    This is a CLI interface function to test one given solution.
    """
    from .extensions import runners

    try:
        from local_config import Config as config
    except ImportError:
        log.warning("Local config loading failed.")
        from config import BaseConfig as config

    for _programming_language_id, _programming_language_name in itertools.chain(
            config.DOTS_LANG_ID_TO_RUNNER.items(),
            config.DOTS_LANG_ID_TO_COMPILER.items()
        ):
        if _programming_language_name == programming_language:
            programming_language_id = _programming_language_id
            break
    else:
        log.error("There is no match for '%s' programming language", programming_language)
        return

    if testing_mode not in runners.RunnersManager.TESTING_MODES:
        log.error("There is no match for '%s' testing mode", testing_mode)
        return

    if programming_language_id in config.DOTS_LANG_ID_TO_COMPILER:
        config.DOTS_LANG_ID_TO_COMPILER = {
            programming_language_id: config.DOTS_LANG_ID_TO_COMPILER[programming_language_id],
        }

    config.DOTS_LANG_ID_TO_RUNNER = {
        programming_language_id: config.DOTS_LANG_ID_TO_RUNNER[programming_language_id],
    }

    app = create_app(config=config)

    with open(solution_source_filepath, 'rb') as solution_source_file:
        solution_source = solution_source_file.read()

    report = app.test_solution(
            programming_language_id,
            solution_source,
            problems_root,
            testing_mode
        )
    log.info("Testing report:\n%s", report)


def create_app(config=None):
    app = Worker()

    if config is None:
        try:
            from local_config import Config as config
        except ImportError:
            log.warning("Local config loading failed.")
            from config import BaseConfig as config
    app.config.from_object(config)

    from . import extensions
    extensions.init_app(app)

    global current_app
    current_app = app
    return app
