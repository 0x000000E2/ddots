def test_many_files(compiler_runner, tmpdir, hello_world_problem):
    solution_path = tmpdir.join('many_files.c')
    solution_path.write(
            '#include <stdio.h>\n'
            '#include <stdlib.h>\n'
            'void inc(char *s, int n) {\n'
            '    int i = 0;\n'
            '    while (s[i] == \'9\') {\n'
            '        s[i] = \'0\';\n'
            '        i++;\n'
            '    }\n'
            '    s[i]++;\n'
            '}\n'
            'int main() {\n'
            '    char s[100];\n'
            '    for (int i = 1; i < 100; i++) s[i] = \'0\';\n'
            '    s[0] = \'1\';\n'
            '    while (1) {\n'
            '        freopen(s, "w", stdout);\n'
            '        inc(s, 100);\n'
            '    }\n'
            '    return 0;\n'
            '}'
        )
    testing_report = compiler_runner(
            'gcc11',
            runner='binary',
            problem_path=hello_world_problem['root'],
            solution_source_path=str(solution_path),
            testing_mode='full'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'TL', '0']
