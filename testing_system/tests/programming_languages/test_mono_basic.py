# encoding: utf-8
import pytest

from worker.exceptions import CompilationError


def test_mono_basic_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.vb')
    OK_solution_path.write(
            'Module Module1\n'
            '  Sub Main()\n'
            '    Dim file as System.IO.StreamWriter\n'
            '    file = My.Computer.FileSystem.OpenTextFileWriter("output.txt", True)\n'
            '    file.WriteLine("Hello World!")\n'
            '    file.Close()\n'
            '  End Sub\n'
            'End Module'
        )
    testing_report = compiler_runner(
            'mono-basic',
            problem_path=hello_world_problem['root'],
            solution_source_path=str(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


def test_mono_basic_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.vb')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'mono-basic',
                problem_path=hello_world_problem['root'],
                solution_source_path=str(CE_solution_path)
            )
    assert 'solution.source (1,4) : error VBNC30203: Identifier expected.' in str(CE.value)
