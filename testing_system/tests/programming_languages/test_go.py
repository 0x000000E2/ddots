# encoding: utf-8
import pytest

from worker.exceptions import CompilationError


def test_go_compiler_OK(compiler, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.go')
    OK_solution_path.write(
            'package main\n'
            'import "fmt"\n'
            'func main() { fmt.Printf("Hello world!\\n") }'
        )
    compiler(
            'go',
            problem_path=hello_world_problem['root'],
            solution_source_path=str(OK_solution_path)
        )

def test_go_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.go')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'go',
                problem_path=hello_world_problem['root'],
                solution_source_path=str(CE_solution_path)
            )
    assert 'solution.source:1:1: expected \'package\', found \'if\'' in str(CE.value)
