# encoding: utf-8
import pytest

from worker.exceptions import CompilationError


def test_rust_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.rs')
    OK_solution_path.write(
            'use std::io::BufWriter;'
            'use std::io::prelude::*;'
            'use std::fs::File;'
            'fn main() {'
            '    let fout = File::create("output.txt");'
            '    let mut fout = BufWriter::new(fout.unwrap());'
            '    write!(&mut fout, "Hello World!");'
            '}'
        )
    testing_report = compiler_runner(
            'rust',
            runner='binary',
            problem_path=hello_world_problem['root'],
            solution_source_path=str(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']

def test_rust_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.rs')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'rust',
                problem_path=hello_world_problem['root'],
                solution_source_path=str(CE_solution_path)
            )
    assert 'solution.source' in str(CE.value)
    assert 'error: expected item, found `if`' in str(CE.value)
