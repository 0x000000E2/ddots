# encoding: utf-8
import pytest

from worker.exceptions import CompilationError


def test_bash_OK(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.sh')
    OK_solution_path.write('echo "Hello World!" > output.txt')
    testing_report = compiler_runner(
            'bash',
            problem_path=hello_world_problem['root'],
            solution_source_path=str(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']


def test_bash_compiler_CE(compiler, tmpdir, hello_world_problem):
    CE_solution_path = tmpdir.join('CE_solution.sh')
    CE_solution_path.write('if')
    with pytest.raises(CompilationError) as CE:
        compiler(
                'bash',
                problem_path=hello_world_problem['root'],
                solution_source_path=str(CE_solution_path)
            )
    assert 'solution.source: line 2: syntax error: unexpected end of file' in str(CE.value)
