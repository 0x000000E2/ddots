# encoding: utf-8
import pytest

from worker.extensions import compilers, runners


class App(object):
    class config(object):
        DOCKER_DAEMON_API_URL = 'unix:///var/run/docker.sock'
        CONTAINER_MEMORY_HARD_LIMIT = 512 * 1024 * 1024
        RUNNER_STACK_HARD_LIMIT = 64 * 1024 * 1024
        BASE_CONTAINER_NAME = 'programming_language_testing'
        SHARED_VOLUMES = {
                'data': {
                        'volume_name': 'ddots-testing-system-testing-data',
                        'mount_point': '/data',
                    },
                'sandbox': {
                        'volume_name': 'ddots-testing-system-testing-sandbox',
                        'mount_point': '/sandbox',
                    },
            }
        CPU_ID = 0
        DOTS_LANG_ID_TO_COMPILER = {}
        DOTS_LANG_ID_TO_RUNNER = {}
        RUNNER_EXTRA_OPTIONS = {}
        RUNNER_TIME_LIMIT_FACTOR = 0.5


@pytest.fixture(scope='session')
def compiler():
    _compilers_manager = compilers.CompilersManager(app=App())
    def _compiler(programming_language_code, **kwargs):
        _compilers_manager.init_container(programming_language_code)
        report = _compilers_manager.start(programming_language_code, **kwargs)
        _compilers_manager.rm_container(programming_language_code)
        return report
    return _compiler


@pytest.fixture(scope='session')
def runner():
    _runners_manager = runners.RunnersManager(app=App())
    def _runner(programming_language_code, **kwargs):
        _runners_manager.init_container(programming_language_code)
        report = _runners_manager.start(programming_language_code, **kwargs)
        _runners_manager.rm_container(programming_language_code)
        return report
    return _runner


@pytest.fixture(scope='session')
def compiler_runner(compiler, runner):
    _runner = runner
    def _compiler_runner(
            programming_language_code, problem_path, solution_source_path, runner=None, **kwargs
        ):
        solution_executable_path = compiler(
                programming_language_code,
                problem_path=problem_path,
                solution_source_path=solution_source_path
            )
        if runner is None:
            runner = programming_language_code
        return _runner(
                runner,
                problem_path=problem_path,
                solution_executable_path=solution_executable_path,
                **kwargs
            )
    return _compiler_runner
