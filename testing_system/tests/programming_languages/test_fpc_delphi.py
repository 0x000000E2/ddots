# encoding: utf-8
import pytest

from worker.exceptions import CompilationError


def test_fpc_integer_is_32bit(compiler_runner, tmpdir, hello_world_problem):
    OK_solution_path = tmpdir.join('OK_solution.pas')
    OK_solution_path.write(
            'var test: integer;'
            'begin'
            '  assign(output, \'output.txt\');'
            '  rewrite(output);'
            '  test := 32767;'
            '  inc(test);'
            '  if test > 0 then'
            '    writeln(\'Hello World!\');'
            'end.'
        )
    testing_report = compiler_runner(
            'fpc-delphi',
            runner='binary',
            problem_path=hello_world_problem['root'],
            solution_source_path=str(OK_solution_path),
            testing_mode='one'
        )
    assert testing_report.split('\n')[0].split(' ')[:3] == ['1', 'OK', '3.8']
