# encoding: utf-8
import pytest

from worker import create_app


@pytest.fixture(scope='session')
def current_app():
    from config import TestingConfig
    app = create_app(TestingConfig)
    return app


@pytest.fixture(
        scope='session',
        params=(
                {'input_file': 'input.txt', 'output_file': 'output.txt'},
                {'input_file': 'stdin', 'output_file': 'stdout'},
            ),
        ids=(
                'hello_world_problem-input.txt/output.txt',
                'hello_world_problem-stdin/stdout',
            )
    )
def hello_world_problem(request, tmpdir_factory):
    hello_world_problem_path = tmpdir_factory.mktemp('hello_world_problem')
    hello_world_problem_id = hello_world_problem_path.basename
    problem_xml_file = hello_world_problem_path.join('Problem.xml')
    problem_xml_file.write(
            '<?xml version="1.0" encoding="windows-1251"?>'
            '<!-- Problem exchange format 0.1 -->'
            '<Problem '
            '        TimeLimit="0.2"'
            '        MemoryLimit="64"'
            '        InputFile="%(input_file)s"'
            '        OutputFile="%(output_file)s"'
            '        CheckerExe="check"'
            '        TestCount="2"'
            '        PointsOnGold="100">'
            '    <Test Input="01.in" Answer="01.out" Points="3.8"/>'
            '    <Test Input="02.in" Answer="02.out" Points="3.8"/>'
            '</Problem>'
            % {
                    'input_file': request.param['input_file'],
                    'output_file': request.param['output_file'],
                }
        )
    hello_world_problem_path.join('01.in').write('')
    hello_world_problem_path.join('01.out').write('Hello World!')
    hello_world_problem_path.join('02.in').write('World!')
    hello_world_problem_path.join('02.out').write('Hello World!')
    checker_file = hello_world_problem_path.join('check')
    checker_file.write(
            '#!/bin/sh\n'
            'INPUT_FILE="$1"\n'
            'OUTPUT_FILE="$2"\n'
            'ANSWER_FILE="$3"\n'
            'OUTPUT="$(cat "$OUTPUT_FILE" 2>/dev/null | sed "s/ *$//")"\n'
            'ANSWER="$(cat "$ANSWER_FILE" 2>/dev/null | sed "s/ *$//")"\n'
            'if [ "$OUTPUT" = "$ANSWER" ]; then exit 0 ; fi\n'
            'if [ "$OUTPUT" = "" ]; then exit 2 ; fi\n'
            'exit 1\n'
        )
    checker_file.chmod(0o755)
    return {
            'problem_id': hello_world_problem_id,
            'path': hello_world_problem_path,
            'root': str(hello_world_problem_path),
            'input_file': request.param['input_file'],
            'output_file': request.param['output_file'],
        }
