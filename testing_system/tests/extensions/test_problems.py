# encoding: utf-8
import os
import tarfile

from worker.extensions import problems


class App(object):
    class config(object):
        PROBLEMS_DB_ROOT = None
        DOTS_PROBLEM_DOWNLOAD_TIMEOUT = 0.1


def test_download_problem_targz_file(tmpdir):
    local_folder = tmpdir.mkdir('1001')
    local_folder.join('Problem.xml').write(
            '<?xml version="1.0" encoding="windows-1251"?><Problem></Problem>'
        )
    download_folder = tmpdir.mkdir('download')
    with tarfile.open(str(download_folder.join('1001.tar.gz')), 'w:gz') as problem_targz_file:
        problem_targz_file.add(str(local_folder), arcname='1001')

    problems.download_problem_targz_file(
            'http://127.0.0.1/1001.tar.gz',
            str(download_folder.join('1001')),
        )

    assert download_folder.join('1001').check()
    assert download_folder.join('1001').join('Problem.xml').check()


def test_ProblemsManager(hello_world_problem):
    class TestApp(App):
        class config(App.config):
            PROBLEMS_DB_ROOT = os.path.dirname(hello_world_problem['root'])

    problems_manager = problems.ProblemsManager(app=TestApp())

    assert problems_manager.get_problem_path(
            hello_world_problem['problem_id']
        ) == hello_world_problem['root']

    problems_manager.prepare_problem(hello_world_problem['problem_id'])
    assert hello_world_problem['path'].check()
    assert hello_world_problem['path'].join('Problem.xml').check()


def test_ProblemsDockerManagerMixin(tmpdir, hello_world_problem):
    class ProblemsDockerManagerMixin(problems.ProblemsDockerManagerMixin):
        _shared_volumes = {
                'data': {
                        'mount_point': str(tmpdir),
                    },
            }

    problems_docker_manager_mixin = ProblemsDockerManagerMixin()
    problems_docker_manager_mixin.prepare_environment(hello_world_problem['root'])

    current_problem_root = tmpdir.join(ProblemsDockerManagerMixin.CURRENT_PROBLEM_DIRNAME)
    assert current_problem_root.join(ProblemsDockerManagerMixin.PROBLEM_XML_NAME).check()
    expected_problem_folder_files = set(os.listdir(hello_world_problem['root']))
    assert set(os.listdir(str(current_problem_root))) == expected_problem_folder_files
