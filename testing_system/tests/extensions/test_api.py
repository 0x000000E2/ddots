# encoding: utf-8

from unittest.mock import patch

from worker.extensions import api


class App(object):
    class config(object):
        DOTS_API_URL = 'http://127.0.0.1/'
        DOTS_API_USE_KEEP_ALIVE_CONNECTIONS = True


class Response(object):
    def __init__(self, status_code, cookies=None, content=None):
        self.status_code = status_code
        self.cookies = cookies
        self.content = content


def test_DOTSApi_authenticate():
    response = Response(status_code=200, cookies={'DSID': 'session_key'})

    dots_api = api.DOTSApi(app=App())
    with patch.object(dots_api.session, 'get', return_value=response) as session_get:
        dots_api.authenticate('username', 'password')

    assert session_get.call_count == 1
    assert '/sess/session_key/bot' in dots_api.base_session_url


def test_DOTSApi_get():
    response = Response(status_code=200, content="test")

    dots_api = api.DOTSApi(app=App())
    with patch.object(dots_api.session, 'get', return_value=response) as session_get:
        dots_api.base_session_url = 'http://127.0.0.1/sess'
        assert dots_api.get('/solution').content == "test"

    assert session_get.call_count == 1
    assert session_get.call_args[0][0] == dots_api.base_session_url + '/solution'
