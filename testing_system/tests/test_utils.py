# encoding: utf-8

from unittest.mock import patch

from worker import utils


def test_download_file(tmpdir):
    from worker.extensions.api import DOTSApi

    class Response(object):
        def __init__(self, status_code, content):
            self.status_code = status_code
            self.content = content

        def iter_content(self, **kwargs):
            # pylint: disable=unused-argument
            return self.content

    local_file_path = tmpdir.join('1.txt')

    response_content = [b"File ", b"Content."]

    response = Response(status_code=200, content=response_content)
    with patch.object(DOTSApi, 'get', return_value=response) as api_get:
        utils.download_file('http://127.0.0.1/1.txt', str(local_file_path), session=DOTSApi())

    assert api_get.call_count == 1
    assert local_file_path.read_binary() == b''.join(response_content)

    # Downloading an existing file should be skipped
    response = Response(status_code=200, content=response_content + ["IGNORED"])
    with patch.object(DOTSApi, 'get', return_value=response) as api_get:
        utils.download_file('http://127.0.0.1/1.txt', str(local_file_path), session=DOTSApi())

    assert not api_get.called
    assert local_file_path.read_binary() == b''.join(response_content)
