#!/bin/sh

# Минимальная реализация patch выглядит вот так:
#cat
# , но мы рассмотрим более интересные варианты:

# Сохраняем текст решения в переменную
solution="$(cat)"

# Через переменные окружения нам предоставляется информация о компиляторе:
#
# + $PROGRAMMING_LANGUAGE не зависит от конкретной версии компилятора, возможные значения:
#   * pascal
#   * c
#   * c++
#   * c#
#   * java
#   * scala
#   * kotlin
#   * go
#   * sh
#   * python
#   * ruby
#   * rust
#   * php
#   * javascript
#   * haskell
#
if [ "$PROGRAMMING_LANGUAGE" = 'pascal' ]; then
    # Допустим, для решений на Pascal по условию не нужно писать Begin/End.,
    # Здесь мы их и добавим в код:
    echo 'Begin'
    echo "$solution"
    echo 'End.'
    exit 0
fi

# + $COMPILER_NAME уникальное имя компилятора, например, fpc, gcc, gcc11, gxx17, python3, haskell
#
if [ "$COMPILER_NAME" = 'gcc' ]; then
    # Здесь мы обработаем код если выбран GCC компилятор, который соответсвует
    # C89 (ANSI C) стандарту, но в это условие не будет попадать C11 (стандарт
    # 2011 года).
    #
    # Давайте усложним решения на C89? Запретим использовать printf, но всё ещё
    # можно использовать puts.
    #
    if ! grep -q 'printf'; then
        # grep нашёл строку printf в коде, выводим сообщение об ошибке
        # компиляции в stderr (>&2) и выходим с exit 1
        echo "solution.source: Testing System Compilation Error: printf is forbidden!" >&2
        exit 1
    fi
    echo "$solution"
    exit 0

    # Альтернативный способ - жестоко удалить из кода все printf, а компилятор
    # там уже сам разберётся и скажет будет ли там CE:
    #echo "$solution" | sed 's/printf//g'
    #exit 0
fi

if [ "$PROGRAMMING_LANGUAGE" = 'c++' ]; then
    # Давайте решениям на С++ будет предоставляться функция print_hello_world(),
    # которая сама выведет правильный ответ. Заготовку кода возьмём из файла.
    cat "$(dirname "$0")/patching/template.cpp"
    echo "$solution"
    exit 0
fi

# Если выполнение дошло до этой точки, мы либо возвращаем исходное решение без изменений:
echo "$solution"

# , либо текст с ошибкой и exit 1:
#echo "solution.source: Testing System Compilation Error: The problem doesn't accept solutions in $PROGRAMMING_LANGUAGE" >&2
#exit 1
